package com.tom.atm;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private EditText edUserid;
    private EditText edPasswd;
    private String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edUserid = findViewById(R.id.ed_userid);
        edPasswd = findViewById(R.id.ed_passwd);
        String userid = getSharedPreferences("atm", MODE_PRIVATE)
                .getString("USERID", "");
        Log.d(TAG, "onCreate: ");
        edUserid.setText(userid);
    }

    public void login(View view){
        userid = edUserid.getText().toString();
        String passwd = edPasswd.getText().toString();
        String login = "http://atm201605.appspot.com/login?uid="+ userid +"&pw="+passwd;
        new LoginTask().execute(login);

        /*if (userid.equals("jack") && passwd.equals("1234")){
            getSharedPreferences("atm", MODE_PRIVATE)
                    .edit()
                    .putString("USERID", userid)
                    .apply();
            setResult(RESULT_OK);
            finish();
        }else{
            new AlertDialog.Builder(this)
                    .setTitle("登入訊息")
                    .setMessage("登入失敗")
                    .setPositiveButton("OK", null)
                    .show();
        }*/
    }

    class LoginTask extends AsyncTask<String, Void, Integer>{

        @Override
        protected Integer doInBackground(String... strings) {
            int data = 0;
            try {
                URL url = new URL(strings[0]);
                InputStream is = url.openStream();
                data = is.read();
                Log.d(TAG, "login: " + data);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (integer == 48){
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle("登入訊息")
                        .setMessage("登入失敗")
                        .setPositiveButton("OK", null)
                        .show();
            }else{
                getSharedPreferences("atm", MODE_PRIVATE)
                        .edit()
                        .putString("USERID", userid)
                        .apply();
                setResult(RESULT_OK);
                finish();
            }
        }
    }

    public void cancel(View view){

    }

}
