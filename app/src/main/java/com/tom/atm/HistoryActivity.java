package com.tom.atm;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HistoryActivity extends AppCompatActivity {

    private static final String TAG = HistoryActivity.class.getSimpleName();
    OkHttpClient client = new OkHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        String url = "http://atm201605.appspot.com/h";
//        new HistoryTask().execute(url);
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String json = response.body().string();
                Log.d(TAG, "onResponse: " + json);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HistoryActivity.this,
                                json, Toast.LENGTH_LONG).show();
                    }
                });
                parseJSON(json);
            }
        });

    }

    private void parseJSON(String json) {
        final List<Transaction> trans = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(json);
            for (int i = 0; i < array.length(); i++) {
                Transaction t = new Transaction(array.getJSONObject(i));
                Log.d(TAG, "parseJSON: " + t);
                trans.add(t);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // setup ListView
        // setupListView(trans);
        // setup RecyclerView
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                RecyclerView recyclerView =
                        findViewById(R.id.recycler);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(HistoryActivity.this));
                TransactionAdapter adapter = new TransactionAdapter(trans);
                recyclerView.setAdapter(adapter);
            }
        });
    }

    class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder>{

        private final List<Transaction> trans;

        public TransactionAdapter(List<Transaction> trans) {
            this.trans = trans;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_tran, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder,
                                     int position) {
            Transaction t = trans.get(position);
            holder.accountTextView.setText(t.getAccount());
            holder.dateTextView.setText(t.getDate());
            holder.amountText.setText(t.getAmount()+"");
        }

        @Override
        public int getItemCount() {
            return trans.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder{
            TextView accountTextView;
            TextView dateTextView;
            TextView amountText;
            public ViewHolder(View itemView) {
                super(itemView);
                accountTextView = itemView.findViewById(R.id.item_account);
                dateTextView = itemView.findViewById(R.id.item_date);
                amountText = itemView.findViewById(R.id.item_amount);
            }
        }

    }

    private void setupListView(final List<Transaction> trans) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListView listView = findViewById(R.id.list);
                HistoryAdapter adapter = new HistoryAdapter(trans);
                listView.setAdapter(adapter);
            }
        });
    }

    class HistoryAdapter extends BaseAdapter{

        private final List<Transaction> trans;

        public HistoryAdapter(List<Transaction> trans) {
            this.trans = trans;
        }

        @Override
        public int getCount() {
            return trans.size();
        }

        @Override
        public Object getItem(int position) {
            return trans.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            Transaction t = trans.get(position);
            if (view == null){
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_tran, null, false);

            }
            TextView accountText = view.findViewById(R.id.item_account);
            TextView dateText = view.findViewById(R.id.item_date);
            TextView amountText = view.findViewById(R.id.item_amount);
            accountText.setText(t.getAccount());
            dateText.setText(t.getDate());
            amountText.setText(t.getAmount()+"");

            return view;
        }
    }

    class HistoryTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... strings) {
            StringBuilder sb = new StringBuilder();
            try {
                URL url = new URL(strings[0]);
                InputStream is = url.openStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader in = new BufferedReader(isr);
                String line = in.readLine();

                while(line != null){
                    sb.append(line);
                    line = in.readLine();
                }
                Log.d(TAG, "sb: " + sb.toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        }
    }
}
