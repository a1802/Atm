package com.tom.atm;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class FinanceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FinanceActivity.this, AddActivity.class));
            }
        });
        ListView listView = findViewById(R.id.list);
        // helper?
        DBHelper helper = DBHelper.getInstance(this);
        // cursor?
        Cursor cursor = helper.getReadableDatabase()
                        .query("exp",
                                null,
                                null, null,
                                null, null, null);
        // adapter?
        String[] from = new String[]{"cdate", "info", "amount"};
        int[] to = new int[]{R.id.item_date, R.id.item_info, R.id.item_amount};
        SimpleCursorAdapter adapter =
                new SimpleCursorAdapter(this,
                        R.layout.item,
                        cursor,
                        from, to,
                        1){
                    @Override
                    public void bindView(View view, Context context, Cursor cursor) {
                        super.bindView(view, context, cursor);
                        int amount = cursor.getInt(cursor.getColumnIndex("amount"));
                        if (amount > 100){
                            /*TextView amountTextView = view.findViewById(R.id.item_amount);
                            amountTextView.setTextColor(getResources().getColor(R.color.amount_warning));*/
                            view.setBackgroundColor(getResources().getColor(R.color.amount_warning));
                        }
                    }
                };

        listView.setAdapter(adapter);
    }

}
