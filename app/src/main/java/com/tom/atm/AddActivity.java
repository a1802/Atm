package com.tom.atm;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddActivity extends AppCompatActivity {

    private static final String TAG = AddActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
    }

    public void add(View view){
        EditText edDate = findViewById(R.id.ed_date);
        EditText edInfo = findViewById(R.id.ed_info);
        EditText edAmount = findViewById(R.id.ed_amount);
        String date = edDate.getText().toString();
        String info = edInfo.getText().toString();
        int amount = Integer.parseInt(edAmount.getText().toString());
        Log.d(TAG, "add: "+ date + "/" + info + "/" + amount);
        //Sqlite
        DBHelper helper = DBHelper.getInstance(this);
        ContentValues values = new ContentValues();
        values.put("cdate", date);
        values.put("info", info);
        values.put("amount", amount);
        long id = helper.getWritableDatabase()
                .insert("exp", null, values);
        if (id == -1){

        }else{
            Toast.makeText(this, "Added", Toast.LENGTH_LONG)
                    .show();
        }
        /*helper.getWritableDatabase().execSQL("insert into " +
                "exp    (cdate,       info, amount) " +
                "values  ('2018-01-01', 'test', 300)");*/
    }
}
