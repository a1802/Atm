package com.tom.atm;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2018/3/25.
 */

public class Transaction {
    String account;
    String date;
    int amount;
    int type;

    public Transaction() {
    }

    public Transaction(String account, String date, int amount, int type) {
        this.account = account;
        this.date = date;
        this.amount = amount;
        this.type = type;
    }

    public Transaction(JSONObject obj) {
        try {
            this.account = obj.getString("account");
            this.date = obj.getString("date");
            this.amount = obj.getInt("amount");
            this.type = obj.getInt("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return account + "/" + date + "/" + amount + "/" + type;
    }
}
