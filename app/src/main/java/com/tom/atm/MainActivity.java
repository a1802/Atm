package com.tom.atm;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int REQUEST_LOGIN = 5;
    private static final String TAG = MainActivity.class.getSimpleName();
    boolean logon = false;
    String[] func = {"餘額查詢", "交易明細", "最新消息", "投資理財", "離開"};
    int[] icons = {R.drawable.func_balance,
            R.drawable.func_history,
            R.drawable.func_news,
            R.drawable.func_finance,
            R.drawable.func_exit};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!logon) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, REQUEST_LOGIN);
        }
        ListView listView = findViewById(R.id.list);
        ArrayAdapter adapter = new ArrayAdapter(
                this, R.layout.item_row, R.id.text1, func);
//                this, android.R.layout.simple_list_item_1, data);
        listView.setAdapter(adapter);
        //Spinner
        Spinner spinner = findViewById(R.id.spinner);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.d(TAG, "onItemSelected: " + position + "/" + func[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //Grid
        GridView gridView = findViewById(R.id.grid);
        IconAdapter iconAdapter = new IconAdapter();
        gridView.setAdapter(iconAdapter);
        gridView.setOnItemClickListener(this);
//        setupRecyclerView();


    }

    private void setupRecyclerView() {
        //RecyclerView
        RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        FuncRecyclerAdapter adapter1 = new FuncRecyclerAdapter();
        recyclerView.setAdapter(adapter1);
    }

    class FuncRecyclerAdapter extends RecyclerView.Adapter<FuncRecyclerAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_row, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.textView.setText(func[position]);
            holder.imageView.setImageResource(icons[position]);
        }

        @Override
        public int getItemCount() {
            return func.length;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView textView;
            ImageView imageView;

            public ViewHolder(View itemView) {
                super(itemView);
                textView = itemView.findViewById(R.id.text1);
                imageView = itemView.findViewById(R.id.image);
            }
        }
    }


    class IconAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return func.length;
        }

        @Override
        public Object getItem(int position) {
            return func[position];
        }

        @Override
        public long getItemId(int position) {
            return icons[position];
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = getLayoutInflater()
                        .inflate(R.layout.item_row, null);
            }
            ImageView imageView = view.findViewById(R.id.image);
            imageView.setImageResource(icons[position]);
            TextView tv = view.findViewById(R.id.text1);
            tv.setText(func[position]);
            return view;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode == RESULT_OK) {

            } else {
                finish();
            }
        }
    }

    public void test(View view) {
        new AlertDialog.Builder(this)
                .setPositiveButton("Login", null)
                .setTitle("Please login")
                .show();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long itemId) {
        Log.d(TAG, "onItemClick: " + position);
        switch ((int) itemId) {
            case R.drawable.func_balance:

                break;
            case R.drawable.func_history:
                startActivity(new Intent(this, HistoryActivity.class));
                break;
            case R.drawable.func_news:
                startActivity(new Intent(this, AsyncActivity.class));
                break;
            case R.drawable.func_finance:
                Intent intent = new Intent(this, FinanceActivity.class);
                startActivity(intent);
                break;
            case R.drawable.func_exit:

                break;
        }
    }
}